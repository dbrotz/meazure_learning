class UserExamWindow < ApplicationRecord
  belongs_to :user 
  belongs_to :exam_window

  validates :user, uniqueness: { scope: :exam_window }
end
