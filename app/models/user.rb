class User < ApplicationRecord
  validates :first_name, presence: true, uniqueness: { scope: :last_name, message: "User already exists with this last and first name." }
  validates :last_name, presence: true
  validates :phone_number, phone: true, uniqueness: true

  has_many :user_exam_windows, dependent: :destroy
  has_many :exam_windows, through: :user_exam_windows
  has_many :exams, through: :exam_windows

  before_save :normalize_name

  def normalize_name
    self.first_name = self.first_name.downcase
    self.last_name = self.last_name.downcase
  end
end
