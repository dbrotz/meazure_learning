class ExamWindow < ApplicationRecord
  belongs_to :exam

  has_many :user_exam_windows, dependent: :destroy
  has_many :users, through: :user_exam_windows
end
