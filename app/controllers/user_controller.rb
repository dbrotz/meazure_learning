class UserController < ApplicationController
  def check_in
    params.require([
      :first_name,
      :last_name,
      :phone_number,
      :college_id,
      :exam_id,
      :start_time,
    ])
    phone_number = Phonelib.parse(params[:phone_number])
    first_name = params[:first_name]&.downcase
    last_name = params[:last_name]&.downcase
    start_time = DateTime.parse(params[:start_time])
    college_id = params[:college_id].to_i
    exam_id = params[:exam_id].to_i

    return render_bad_request("Invalid phone_number") if phone_number.invalid?
    return render_bad_request("Invalid college_id") if College.find_by_id(college_id).nil?

    user = User.find_or_initialize_by(first_name: first_name, last_name: last_name, phone_number: phone_number.sanitized)
    return render_bad_request("Invalid user") if user.nil?

    # Add UserExamWindow if the User has just been created.
    unless user.persisted?
      return render_bad_request("Invalid user") unless user.save

      UserExamWindow.create!(user: user, exam_window_id: exam_id)
    end
    exam_window = user.exam_windows.find_by(exam_id: exam_id)
    return render_bad_request("Exam has no exam windows") if exam_window.nil?

    # start_time in between exam window's start_time and end_time.
    in_range = exam_window.end_time > start_time && exam_window.start_time < start_time
    valid_college = exam_window.exam.college_id == college_id
    return render_bad_request("Invalid start_time") unless exam_window.present? && valid_college && in_range

    render status: :ok
  rescue Date::Error
    render_bad_request("Invaid start_time")
  end
end
