class ApplicationController < ActionController::API
  before_action :initialize_log
  after_action :save_log

  protected

  def render_bad_request(message)
    @api_request.error = message
    render status: :bad_request, json: { error: message }
  end

  def initialize_log
    endpoint = "#{params[:controller]}\##{params[:action]}"
    @api_request = ApiRequest.new(payload: JSON.parse(request.body.read), remote_ip: request.remote_ip, endpoint: endpoint)
  end

  def save_log
    @api_request.save
  end
end
