class CreateUserExamWindows < ActiveRecord::Migration[7.0]
  def change
    create_table :user_exam_windows do |t|
      t.references :user, null: false, foreign_key: true
      t.references :exam_window, null: false, foreign_key: true

      t.timestamps
    end
  end
end
