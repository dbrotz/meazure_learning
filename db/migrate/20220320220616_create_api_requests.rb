class CreateApiRequests < ActiveRecord::Migration[7.0]
  def change
    create_table :api_requests do |t|
      t.json :payload
      t.string :remote_ip
      t.string :endpoint
      t.string :error

      t.index :remote_ip
      t.index :endpoint
      t.timestamps
    end
  end
end
